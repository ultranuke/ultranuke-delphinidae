﻿
namespace UltraNuke.Delphinidae
{
    public interface IAggregateRoot : IEntity
    {
        #region Properties
        AggregateState AggregateState { get; set; }
        #endregion
    }
}
