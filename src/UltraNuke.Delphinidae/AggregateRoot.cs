﻿using System;

namespace UltraNuke.Delphinidae
{
    public abstract class AggregateRoot : IAggregateRoot
    {
        #region Private Fields
        private Guid id;
        private AggregateState aggregateState;
        #endregion

        #region Constructors
        public AggregateRoot()
            : this(Guid.NewGuid())
        {
        }

        public AggregateRoot(Guid id)
        {
            this.id = id;
        }
        #endregion

        #region IAggregateRoot Members
        public virtual AggregateState AggregateState
        {
            get { return this.aggregateState; }
            set { this.aggregateState = value; }
        }
        #endregion

        #region IEntity Members
        public virtual Guid Id
        {
            get { return this.id; }
            set { this.id = value; }
        }
        #endregion        
    }
}
